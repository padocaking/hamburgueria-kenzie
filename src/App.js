import './App.css';
import { useState } from 'react'

import Product from './components/Product'
import MenuContainer from './components/MenuContainer'

import backgroundImg from './images/backgroundimg.png'
import logo from './images/kenzieLogo.jpg'
import menuLogo from './images/logo.png'
import { AiOutlineShopping } from 'react-icons/ai'
import { AiOutlineClose } from 'react-icons/ai'
import { AiOutlineSearch } from 'react-icons/ai'

function App() {
  const allProducts = [
    { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 7.99 },
    { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 8.99 },
    { id: 3, name: 'X-Salada', category: 'Sanduíches', price: 10.99 },
    { id: 4, name: 'Big Kenzie', category: 'Sanduíches', price: 16.99 },
    { id: 5, name: 'Batata Frita', category: 'Acompanhamentos', price: 8.99 },
    { id: 6, name: 'Guaraná', category: 'Bebidas', price: 4.99 },
    { id: 7, name: 'Coca', category: 'Bebidas', price: 4.99 },
    { id: 8, name: 'Fruki', category: 'Bebidas', price: 4.99 }
  ]

  const [currentSale, setCurrentSale] = useState([])
  const [cartTotal, setCartTotal] = useState(0)

  function handleClick(productId, productPrice) {
    setCarrinho(true)
    if (currentSale.map((product) => product.id).includes(productId) === false) {
      setCartTotal(cartTotal + productPrice)
      setCurrentSale([...currentSale, allProducts.find((product) => product.id === productId)])
    }
  }
  function remove(productId, productPrice) {
    setCartTotal(cartTotal - productPrice)
    setCurrentSale(currentSale.filter((product) => product.id !== productId))
  }

  // Carrinho Sidebar Function
  const [carrinho, setCarrinho] = useState(false)
  function carrinhoOpen() {
    setCarrinho(true)
  }
  function carrinhoClosed() {
    setCarrinho(false)
  }

  return (
    <div className="App">
      
      <header>
        <picture>
          <img src={menuLogo} alt="Kenzie Logo"></img>
        </picture>
        <form className="divSearch menu">
            <AiOutlineSearch />
            <input type="text" placeholder="Buscar" className="search" />
            <button type="button"></button>
        </form>
        <AiOutlineShopping onClick={carrinhoOpen}/>
      </header>

        <div className={carrinho ? 'carrinho open' : 'carrinho'}>
          <div className="order">
            <AiOutlineClose onClick={carrinhoClosed} />
            <h2>Seu pedido:</h2>
          </div>

          <div className="saleContainer">
            {currentSale.map((product) => (
              <Product prop={product} saleClass remove={remove}/>
            ))}
          </div>

          <div className="finalizarCompra">
              <h3>Total: R$ {cartTotal.toFixed(2)}</h3>
              <button>Finalizar Compra</button>
          </div>
        </div>

      <div className="content">
          <picture className="backgroundImg">
          <img src={backgroundImg} alt="Background Img"></img>
        </picture>

        <div className="restaurantInfo">
          <picture className="logo">
            <img src={logo} alt="Kenzie Logo"></img>
          </picture>
          <h2>Kenzie Burger</h2>
        </div>
        
        <MenuContainer products={allProducts} handleClick={handleClick}/>

      </div>
      <footer></footer>
    </div>
  );
}

export default App;