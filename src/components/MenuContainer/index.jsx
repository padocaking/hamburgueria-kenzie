import './style.css'
import { useState } from 'react'
import Product from '../Product'

import { AiOutlineSearch } from 'react-icons/ai'
import { AiOutlineClose } from 'react-icons/ai'

function MenuContainer(props) {

        // Filter & Display Product Function
    const [products, setProducts] = useState(props.products);
    const [searchTag, setSearchTag] = useState(false)
    const [userInput, setUserInput] = useState('')

    function showProducts() {
        return products.map((product) => (
          <Product prop={product} handleClick={props.handleClick} />
        ))
    }
    
    function filterProducts() {
      setSearchTag(true)
      userInput === '' ?
      (setProducts(props.products)) :
      (setProducts(props.products.filter((product) => {
        let input = userInput.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[\W_]+/gm,"").toLowerCase()
        let name = product.name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[\W_]+/gm,"").toLowerCase()
        let category = product.category.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[\W_]+/gm,"").toLowerCase()
        return input === name || input === category
      }
        )))
    }
  
    function resetFilter() {
      setSearchTag(false)
      setProducts(props.products)
    }

    return (
        <div>
            <form className="divSearch">
                <AiOutlineSearch />
                <input type="text" placeholder="Buscar no cardápio" className="search"
                value={userInput} onChange={(event) => setUserInput(event.target.value)}/>
                <button type="button" onClick={filterProducts}>Pesquisar</button>
            </form>

            <div className="destaque">
                <h1>Destaques</h1>
                <span className={searchTag ? "searchTag" : "searchTag hide"}>{userInput}
                <AiOutlineClose onClick={resetFilter}/></span>
            </div>

            <main>
                {showProducts()}
            </main>

        </div>
    )
}

export default MenuContainer