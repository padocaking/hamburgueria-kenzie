import './style.css'

import hamburguer from '../../images/hamburguer.jpg'
import xburguer from '../../images/xburguer.png'
import xsalada from '../../images/xsalada.jpg'
import bigkenzie from '../../images/bigkenzie.png'
import batatafrita from '../../images/batatafrita.png'
import guarana from '../../images/guarana.png'
import coca from '../../images/coca-cola.png'
import fruki from '../../images/fruki.png'

import { BiTrash } from 'react-icons/bi'

function Product (props) {
    const imagesForEach = [
        { id: 1, image: hamburguer },
        { id: 2, image: xburguer },
        { id: 3, image: xsalada },
        { id: 4, image: bigkenzie },
        { id: 5, image: batatafrita },
        { id: 6, image: guarana },
        { id: 7, image: coca },
        { id: 8, image: fruki}
    ]
    function returnImage(prop) {
        return imagesForEach.filter((item) => item.id === prop.id)[0].image
    }

    return (
        <div className={props.saleClass ? "saleProductContainer" : "productContainer"}>
            <picture>
                <img src={returnImage(props.prop)} alt="Burger"></img>
            </picture>
            <h2>{props.prop.name}</h2>
            <span className="category">Categoria - {props.prop.category}</span>
            <span className="price"><strong>Preço - R$ {props.prop.price}</strong></span>
            {props.saleClass ?
            <BiTrash onClick={() => props.remove(props.prop.id, props.prop.price)} className="remove" />
            :
            <button onClick={() => props.handleClick(props.prop.id, props.prop.price)}>Adicionar</button>}
            
        </div>
    )
}

export default Product